﻿using System;
using System.Collections.Generic;

//Weighted random selector
public class LootObject
{
    public string Name;
    public double Weight;
    public List<string> Types;
    public string Variant;
    public int minQuant = 1;
    public int maxQuant = 1;

    //Run commands when fished here?

    public LootObject(string name, double weight, params string[] types)
    {
        Name = name;
        Weight = weight;
        Types = new List<string>(types);
    }

    public LootObject(string name, string variant, double weight, params string[] types)
    {
        Name = name;
        Variant = variant;
        Weight = weight;
        Types = new List<string>(types);
    }

    public LootObject(string name, int minQuant, int maxQuant, double weight, params string[] types)
    {
        Name = name;
        this.minQuant = minQuant;
        this.maxQuant = maxQuant;
        Weight = weight;
        Types = new List<string>(types);
    }

    public LootObject(string name, string variant, int minQuant, int maxQuant, double weight, params string[] types)
    {
        Name = name;
        Variant = variant;
        this.minQuant = minQuant;
        this.maxQuant = maxQuant;
        Weight = weight;
        Types = new List<string>(types);
    }
}

public class WeightedRandomSelector
{
    private readonly Random random;
    public readonly List<LootObject> items;

    public WeightedRandomSelector()
    {
        random = new Random();
        items = new List<LootObject>();
    }

    public WeightedRandomSelector AddItem(string code, double weight, params string[] types)
    {
        items.Add(new LootObject(code, weight, types));
        return this;
    }

    public WeightedRandomSelector AddItem(string code, string variant, double weight, params string[] types)
    {
        items.Add(new LootObject(code, variant, weight, types));
        return this;
    }

    public WeightedRandomSelector AddItem(string code, int minQuant, int maxQuant, double weight, params string[] types)
    {
        items.Add(new LootObject(code, minQuant, maxQuant, weight, types));
        return this;
    }

    public WeightedRandomSelector AddItem(string code, string variant, int minQuant, int maxQuant, double weight, params string[] types)
    {
        items.Add(new LootObject(code, variant, minQuant, maxQuant, weight, types));
        return this;
    }

    public LootObject SelectRandomItem(Dictionary<string, double> typeMultipliers = null)
    {
        if (items.Count == 0)
            throw new InvalidOperationException("No items have been added.");

        //Calculate the total weight of items with new values
        double totalWeight = 0.0;
        foreach (LootObject item in items)
        {
            double itemWeight = item.Weight;

            if (typeMultipliers != null)
            {
                foreach (string itemType in item.Types)
                {
                    if (typeMultipliers.TryGetValue(itemType, out double typeMultiplier))
                    {
                        itemWeight *= typeMultiplier;
                    }
                }
            }

            totalWeight += itemWeight;
        }

        if (totalWeight == 0)
            throw new InvalidOperationException("No items found with the specified type multipliers.");

        //Multiply the selected item weight by the type multiplier, if the cumulative weight is within that weight range return it
        double randomValue = random.NextDouble() * totalWeight;
        double cumulativeWeight = 0.0;

        foreach (LootObject item in items)
        {
            double itemWeight = item.Weight;

            if (typeMultipliers != null)
            {
                foreach (string itemType in item.Types)
                {
                    if (typeMultipliers.TryGetValue(itemType, out double typeMultiplier))
                    {
                        itemWeight *= typeMultiplier;
                    }
                }
            }

            cumulativeWeight += itemWeight;
            if (randomValue < cumulativeWeight)
                return item;
        }

        //Return the last item if nothing is found, however this shouldn't be reached
        return items[items.Count - 1];
    }
}

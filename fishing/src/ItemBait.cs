﻿using System.Text;
using Vintagestory.API.Common;

public class ItemBait : Item
{
    public override void GetHeldItemInfo(ItemSlot inSlot, StringBuilder dsc, IWorldAccessor world, bool withDebugInfo)
    {
        base.GetHeldItemInfo(inSlot, dsc, world, withDebugInfo);

        if (inSlot.Itemstack.Collectible.Attributes == null) return;

        dsc.AppendLine("Bait Power: " + inSlot.Itemstack.Collectible.Attributes["baitPower"].AsInt());
        dsc.AppendLine("Consume Chance: " + inSlot.Itemstack.Collectible.Attributes["breakChance"].AsFloat() * 100 + "%");

        //??? too verbose
        if (inSlot.Itemstack.Item.LastCodePart() == "net")
        {
            dsc.AppendLine("Improves chances of inanimate objects");
        }
        if (inSlot.Itemstack.Item.LastCodePart() == "chum")
        {
            dsc.AppendLine("Attracts carnivorous animals");
        }
        if (inSlot.Itemstack.Item.LastCodePart() == "jonaslure")
        {
            dsc.AppendLine("Attracts anomalies in deep water");
        }
    }
}

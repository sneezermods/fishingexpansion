﻿using Vintagestory.API.Common;

public class ItemLootCrate : Item
{
    FishingMod mod;

    public override void OnLoaded(ICoreAPI api)
    {
        base.OnLoaded(api);
        mod = (FishingMod)api.ModLoader.GetModSystem("FishingMod");
    }

    public override void OnHeldInteractStart(ItemSlot slot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel, bool firstEvent, ref EnumHandHandling handling)
    {
        handling = EnumHandHandling.PreventDefault;
        if (api.World.Side == EnumAppSide.Server)
        {
            string type = slot.Itemstack.Collectible.Code.ToString();

            //Spawn 3 items
            for (int i = 0;  i < 3; i++) 
            { 
                LootObject loot = null;
                if (type.Contains("normal"))
                {
                    loot = mod.normalCrate.SelectRandomItem();
                }
                else if (type.Contains("copper"))
                {
                    loot = mod.copperCrate.SelectRandomItem();
                }
                else if (type.Contains("tungsten"))
                {
                    loot = mod.tungstenCrate.SelectRandomItem();
                }

                AssetLocation newAsset = new AssetLocation(loot.Name);

                ItemStack tempStack = null;
                if (byEntity.World.GetItem(newAsset) != null)
                {
                    tempStack = new ItemStack(byEntity.World.GetItem(newAsset));
                }
                else
                {
                    tempStack = new ItemStack(byEntity.World.GetBlock(newAsset));
                }

                //If a variant exists add it
                if (loot.Variant != null)
                {
                    tempStack.Attributes.SetString("type", loot.Variant);
                }

                tempStack.StackSize = api.World.Rand.Next(loot.minQuant, loot.maxQuant);
                byEntity.World.SpawnItemEntity(tempStack, byEntity.ServerPos.XYZ);
            }
        }
        slot.TakeOut(1);
    }
}

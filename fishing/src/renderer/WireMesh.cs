﻿using System;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.MathTools;
using Vintagestory.API.Util;

//Credit: vonzeeple / signals mod
class WireMesh
{
    static float Catenary(float x, float d = 1, float a = 2)
    {
        return a * ((float)Math.Cosh((x - (d / 2)) / a) - (float)Math.Cosh((d / 2) / a));
    }

    static public MeshData MakeWireMesh(Vec3f pos1, Vec3f pos2)
    {
        float t = 0.015f; //Thickness of the wire
        Vec3f dPos = pos2 - pos1; //Distance vector between 2 points
        float dist = pos2.DistanceTo(pos1); //Absolute distance between 2 points

        int nSec = (int)Math.Floor(dist * 2); //Number of sections
        nSec = nSec > 15 ? nSec : 15; //Maximum 15 sections

        //Initialize meshes
        MeshData mesh = new MeshData(4, 6);
        mesh.SetMode(EnumDrawMode.Triangles);

        MeshData mesh_top = new MeshData(4, 6);
        mesh_top.SetMode(EnumDrawMode.Triangles);

        MeshData mesh_bot = new MeshData(4, 6);
        mesh_bot.SetMode(EnumDrawMode.Triangles);

        MeshData mesh_side = new MeshData(4, 6);
        mesh_side.SetMode(EnumDrawMode.Triangles);

        MeshData mesh_side2 = new MeshData(4, 6);
        mesh_side2.SetMode(EnumDrawMode.Triangles);

        //Out of plane translation vector
        Vec3f b = new Vec3f(-dPos.Z, 0, dPos.X).Normalize();

        if (dPos.Z == 0 && dPos.X == 0)
        {
            b = new Vec3f(1, 0, 0);
        }

        Vec3f pos;

        mesh_top.Flags.Fill(0);
        mesh_bot.Flags.Fill(0);
        mesh_side.Flags.Fill(0);
        mesh_side2.Flags.Fill(0);

        //Add vertices
        for (int j = 0; j <= nSec; j++)
        {
            float x = dPos.X / nSec * j;
            float y = dPos.Y / nSec * j;
            float z = dPos.Z / nSec * j;
            float l = (float)Math.Sqrt(x * x + y * y + z * z);
            float dy = Catenary(l / dist, 1, 0.4f); //a variable affects sagginess
            pos = new Vec3f(x, y + dy, z);

            float du = dist / 2 / t / nSec;
            int color = 1;

            /*
            mesh_top.AddVertex(pos1.X + pos.X - b.X * t, pos1.Y + pos.Y + t, pos1.Z + pos.Z - b.Z * t, j * du, 0, color);
            mesh_top.AddVertex(pos1.X + pos.X + b.X * t, pos1.Y + pos.Y + t, pos1.Z + pos.Z + b.Z * t, j * du, 1, color);

            mesh_bot.AddVertex(pos1.X + pos.X - b.X * t, pos1.Y + pos.Y - t, pos1.Z + pos.Z - b.Z * t, j * du, 0, color);
            mesh_bot.AddVertex(pos1.X + pos.X + b.X * t, pos1.Y + pos.Y - t, pos1.Z + pos.Z + b.Z * t, j * du, 1, color);

            mesh_side.AddVertex(pos1.X + pos.X - b.X * t, pos1.Y + pos.Y + t, pos1.Z + pos.Z - b.Z * t, j * du, 1, color);
            mesh_side.AddVertex(pos1.X + pos.X - b.X * t, pos1.Y + pos.Y - t, pos1.Z + pos.Z - b.Z * t, j * du, 0, color);

            mesh_side2.AddVertex(pos1.X + pos.X + b.X * t, pos1.Y + pos.Y + t, pos1.Z + pos.Z + b.Z * t, j * du, 1, color);
            mesh_side2.AddVertex(pos1.X + pos.X + b.X * t, pos1.Y + pos.Y - t, pos1.Z + pos.Z + b.Z * t, j * du, 0, color);
            */

            mesh_top.AddVertex(pos1.X + pos.X - b.X * t, pos1.Y + pos.Y + t, pos1.Z + pos.Z - b.Z * t, j * du, 0, color);
            mesh_top.AddVertex(pos1.X + pos.X + b.X * t, pos1.Y + pos.Y + t, pos1.Z + pos.Z + b.Z * t, j * du, 1, color);

            mesh_bot.AddVertex(pos1.X + pos.X - b.Z * t - b.X * t, pos1.Y + pos.Y - t, pos1.Z + pos.Z + b.X * t - b.Z * t, j * du, 0, color);
            mesh_bot.AddVertex(pos1.X + pos.X - b.Z * t + b.X * t, pos1.Y + pos.Y - t, pos1.Z + pos.Z + b.X * t + b.Z * t, j * du, 1, color);

            mesh_side.AddVertex(pos1.X + pos.X - b.X * t, pos1.Y + pos.Y + t, pos1.Z + pos.Z - b.Z * t, j * du, 1, color);
            mesh_side.AddVertex(pos1.X + pos.X - b.Z * t - b.X * t, pos1.Y + pos.Y - t, pos1.Z + pos.Z + b.X * t - b.Z * t, j * du, 0, color);

            mesh_side2.AddVertex(pos1.X + pos.X + b.X * t, pos1.Y + pos.Y + t, pos1.Z + pos.Z + b.Z * t, j * du, 1, color);
            mesh_side2.AddVertex(pos1.X + pos.X - b.Z * t + b.X * t, pos1.Y + pos.Y - t, pos1.Z + pos.Z + b.X * t + b.Z * t, j * du, 0, color);


            mesh_top.Flags[2 * j] = VertexFlags.PackNormal(new Vec3f(0, 1, 0));
            mesh_top.Flags[2 * j + 1] = VertexFlags.PackNormal(new Vec3f(0, 1, 0));

            mesh_bot.Flags[2 * j] = VertexFlags.PackNormal(new Vec3f(0, -1, 0));
            mesh_bot.Flags[2 * j + 1] = VertexFlags.PackNormal(new Vec3f(0, -1, 0));

            mesh_side.Flags[2 * j] = VertexFlags.PackNormal(-b.X, -b.Y, -b.Z);
            mesh_side.Flags[2 * j + 1] = VertexFlags.PackNormal(-b.X, -b.Y, -b.Z);

            mesh_side2.Flags[2 * j] = VertexFlags.PackNormal(b);
            mesh_side2.Flags[2 * j + 1] = VertexFlags.PackNormal(b);
        }

        //Add indices
        for (int j = 0; j < nSec; j++)
        {
            //Upper stripe
            int offset = 2 * j;
            mesh_top.AddIndex(offset);
            mesh_top.AddIndex(offset + 3);
            mesh_top.AddIndex(offset + 2);
            mesh_top.AddIndex(offset);
            mesh_top.AddIndex(offset + 1);
            mesh_top.AddIndex(offset + 3);

            //Lower Stripe
            mesh_bot.AddIndex(offset);
            mesh_bot.AddIndex(offset + 3);
            mesh_bot.AddIndex(offset + 1);
            mesh_bot.AddIndex(offset);
            mesh_bot.AddIndex(offset + 2);
            mesh_bot.AddIndex(offset + 3);

            //Sides
            mesh_side.AddIndex(offset);
            mesh_side.AddIndex(offset + 3);
            mesh_side.AddIndex(offset + 1);
            mesh_side.AddIndex(offset);
            mesh_side.AddIndex(offset + 2);
            mesh_side.AddIndex(offset + 3);

            mesh_side2.AddIndex(offset);
            mesh_side2.AddIndex(offset + 3);
            mesh_side2.AddIndex(offset + 2);
            mesh_side2.AddIndex(offset);
            mesh_side2.AddIndex(offset + 1);
            mesh_side2.AddIndex(offset + 3);
        }

        mesh.AddMeshData(mesh_top);
        mesh.AddMeshData(mesh_bot);
        mesh.AddMeshData(mesh_side);
        mesh.AddMeshData(mesh_side2);
        mesh.Rgba.Fill((byte)255);

        return mesh;
    }
}
﻿using System;
using System.Collections.Generic;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.MathTools;

//Credit: vonzeeple / signals mod
public class FishingLineRenderer : IRenderer
{
    public double RenderOrder => 0.5;
    public int RenderRange => 100;
    ICoreClientAPI capi;
    int chunkSize = 32;
    public Matrixf ModelMat = new Matrixf();
    Dictionary<Vec3i, MeshRef> MeshRefPerChunk;
    Dictionary<long, long> hookMapIds;

    public FishingLineRenderer(ICoreClientAPI capi, Dictionary<long, long> hookMapIds)
    {
        this.capi = capi;
        this.hookMapIds = hookMapIds;
    }

    public void UpdateWiresMesh()
    {
        IBlockAccessor accessor = capi?.World?.BlockAccessor;
        IClientWorldAccessor world = capi?.World;
        if (hookMapIds == null || accessor == null) return;

        Dictionary<Vec3i, MeshData> MeshPerChunk = new Dictionary<Vec3i, MeshData>();

        //Dispose old meshes
        if (MeshRefPerChunk != null)
        {
            foreach (MeshRef meshRef in MeshRefPerChunk.Values)
            {
                meshRef?.Dispose();
            }
        }
        MeshRefPerChunk = new Dictionary<Vec3i, MeshRef>();

        foreach (KeyValuePair<long, long> entry in hookMapIds)
        {
            EntityPlayer player = (EntityPlayer)capi.World.GetEntityById(entry.Key);
            Entity hook = capi.World.GetEntityById(entry.Value);

            if (player == null || hook == null || player.Pos.XYZ.DistanceTo(capi.World.Player.Entity.Pos.XYZ) > 100) continue;

            /*
            float yoffset = 0;

            if (player.AnimManager.IsAnimationActive("sitflooridle") || player.AnimManager.IsAnimationActive("sitidle") || player.AnimManager.IsAnimationActive("sitidle2"))
            {
                yoffset = -1;
            }
            */

            //Line color here
            if (player.Player.InventoryManager.ActiveHotbarSlot.Itemstack == null || !(player.Player.InventoryManager.ActiveHotbarSlot.Itemstack.Collectible.FirstCodePart() == "fishingpole")) continue;
            
            //Position of the first connection
            EntityPos playerPos = player.Pos;
            Vec3i chunkPos = new Vec3i((int)playerPos.X / chunkSize, (int)playerPos.Y / chunkSize, (int)playerPos.Z / chunkSize); //Chunk which the connection is in

            //Vec3d offset = playerPos.XYZ.AheadCopy(2, player.SidedPos.Pitch, player.SidedPos.Yaw);
            //Vec3d offsetVector = new Vec3d(offset.X, 0, offset.Z).Sub(playerPos.X, 0, playerPos.Z).Normalize();

            //Hook and player positions
            Vec3f pos1;
            if (capi.World.Player.Entity == player && capi.World.Player.CameraMode == EnumCameraMode.FirstPerson) //Initial pos here if in first person
            {
                pos1 = player.CameraPos.AddCopy((-chunkPos.X * chunkSize) + (Math.Cos(capi.World.Player.CameraYaw) * 0.2d) + ((Math.Sin(capi.World.Player.CameraYaw) * 0.2d)),
                                                (-chunkPos.Y * chunkSize) + 2.3d,
                                                (-chunkPos.Z * chunkSize) + (Math.Sin(capi.World.Player.CameraYaw) * -0.2d) + ((Math.Cos(capi.World.Player.CameraYaw) * 0.2d))).ToVec3f();
            }
            else                                                                                                  //Initial pos here if in third person
            {
                //pos1 = player.Pos.XYZ.ToVec3f().AddCopy(-chunkPos.X * chunkSize, (-chunkPos.Y * chunkSize) + 1f, -chunkPos.Z * chunkSize); //Without adjustments
                pos1 = player.Pos.XYZ.Add((-chunkPos.X * chunkSize) + (Math.Cos(player.BodyYaw) * 2f) + (Math.Sin(player.BodyYaw) * 0.4f),
                                          (-chunkPos.Y * chunkSize) + 2.3d,
                                          (-chunkPos.Z * chunkSize) - (Math.Sin(player.BodyYaw) * 2f) + (Math.Cos(player.BodyYaw) * 0.4f)).ToVec3f();
            }

            Vec3f pos2 = hook.Pos.XYZ.Add(-chunkPos.X * chunkSize, -chunkPos.Y * chunkSize, -chunkPos.Z * chunkSize).ToVec3f();
     
            if (MeshPerChunk.ContainsKey(chunkPos))
            {
                MeshData newMesh = WireMesh.MakeWireMesh(pos1, pos2);
                MeshPerChunk[chunkPos].AddMeshData(newMesh);
            }
            else
            {
                MeshPerChunk[chunkPos] = WireMesh.MakeWireMesh(pos1, pos2);
            }
        }

        foreach (KeyValuePair<Vec3i, MeshData> mesh in MeshPerChunk)
        {
            mesh.Value.SetMode(EnumDrawMode.Triangles);
            MeshRefPerChunk[mesh.Key] = capi.Render.UploadMesh(mesh.Value);
        }
        MeshPerChunk.Clear();
    }

    //Renderer
    public void OnRenderFrame(float deltaTime, EnumRenderStage stage)
    {
        UpdateWiresMesh();

        if (MeshRefPerChunk == null) return;
        if (stage != EnumRenderStage.Opaque) return;

        IRenderAPI rpi = capi.Render;
        IClientWorldAccessor worldAccess = capi.World;
        Vec3d camPos = worldAccess.Player.Entity.CameraPos;

        rpi.GLEnableDepthTest();
        rpi.GlEnableCullFace();
        IStandardShaderProgram prog = rpi.PreparedStandardShader(0, 0, 0);
        prog.Use();

        AssetLocation wireTexName = new AssetLocation("block/metal/plate/silver.png");
        int texid = capi.Render.GetOrLoadTexture(wireTexName);
        rpi.BindTexture2d(texid);

        prog.ProjectionMatrix = rpi.CurrentProjectionMatrix;
        prog.ViewMatrix = rpi.CameraMatrixOriginf;
        prog.ModelMatrix = ModelMat.Values;

        foreach (KeyValuePair<Vec3i, MeshRef> mesh in MeshRefPerChunk)
        {
            Vec3d offset = new Vec3d(mesh.Key.X * chunkSize, mesh.Key.Y * chunkSize, mesh.Key.Z * chunkSize);
            prog.ModelMatrix = ModelMat.Identity().Translate(offset.X - camPos.X, offset.Y - camPos.Y, offset.Z - camPos.Z).Values;
            rpi.RenderMesh(mesh.Value);
        }

        prog.Stop();
    }

    public void Dispose()
    {
        capi.Event.UnregisterRenderer(this, EnumRenderStage.Opaque);
        if (MeshRefPerChunk == null) return;
        foreach (MeshRef meshRef in MeshRefPerChunk.Values)
        {
            meshRef?.Dispose();
        }
    }
}

﻿using HarmonyLib;
using ProtoBuf;
using System.Collections.Generic;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Server;
using Vintagestory.API.Util;

public class FishingMod : ModSystem
{
    public readonly string MODID = "fishing";

    //Map of all hook entities to players
    public Dictionary<long, long> hookMapIds = new Dictionary<long, long>();

    ICoreClientAPI capi;
    ICoreServerAPI sapi;
    IServerNetworkChannel serverChannel;

    //Loot pools
    public WeightedRandomSelector regularWaterPool;
    public WeightedRandomSelector saltWaterPool;
    public WeightedRandomSelector boilingWaterPool;
    public WeightedRandomSelector lavaPool;

    //Crate pools
    public WeightedRandomSelector normalCrate;
    public WeightedRandomSelector copperCrate;
    public WeightedRandomSelector tungstenCrate;

    public override void Start(ICoreAPI api)
    {
        base.Start(api);
        api.RegisterItemClass("fishingpole", typeof(ItemFishingPole));
        api.RegisterItemClass("bait", typeof(ItemBait));
        api.RegisterItemClass("lootcrate", typeof(ItemLootCrate));
        api.RegisterEntity("EntityFishingHook", typeof(EntityFishingHook));

        api.RegisterEntity("EntityLeviathanHead", typeof(EntityLeviathanHead));
        api.RegisterEntity("EntityLeviathanSegment", typeof(EntityLeviathanSegment));
    }

    public override void StartClientSide(ICoreClientAPI api)
    {
        base.StartClientSide(api);
        capi = api as ICoreClientAPI;

        capi.Network.RegisterChannel("fishingchannel").RegisterMessageType(typeof(SyncHooksMessage))
            .SetMessageHandler<SyncHooksMessage>(OnServerMessage)
            .RegisterMessageType(typeof(FishingSoundMessage))
            .SetMessageHandler<FishingSoundMessage>(playSound)
            .RegisterMessageType(typeof(ParticlesMessage))
            .SetMessageHandler<ParticlesMessage>(receiveParticles);

        capi.Event.RegisterRenderer(new FishingLineRenderer(capi, hookMapIds), EnumRenderStage.Opaque);
    }

    public override void StartServerSide(ICoreServerAPI api)
    {
        base.StartServerSide(api);
        sapi = api as ICoreServerAPI;

        serverChannel = sapi.Network.RegisterChannel("fishingchannel").RegisterMessageType(typeof(SyncHooksMessage)).RegisterMessageType(typeof(FishingSoundMessage)).RegisterMessageType(typeof(ParticlesMessage));

        regularWaterPool = new WeightedRandomSelector();
        saltWaterPool = new WeightedRandomSelector();
        boilingWaterPool = new WeightedRandomSelector();
        lavaPool = new WeightedRandomSelector();

        normalCrate = new WeightedRandomSelector();
        copperCrate = new WeightedRandomSelector();
        tungstenCrate = new WeightedRandomSelector();

        //Lake pool
        if (api.ModLoader.IsModEnabled("primitivesurvival"))
        {
            regularWaterPool.AddItem("primitivesurvival:psfish-salmon-raw", 1600);
            regularWaterPool.AddItem("primitivesurvival:psfish-catfish-raw", 2500);
            regularWaterPool.AddItem("primitivesurvival:psfish-trout-raw", 1600);
            regularWaterPool.AddItem("primitivesurvival:psfish-perch-raw", 1600);
            regularWaterPool.AddItem("primitivesurvival:psfish-carp-raw", 1600);
            regularWaterPool.AddItem("primitivesurvival:psfish-bass-raw", 1600);
            regularWaterPool.AddItem("primitivesurvival:psfish-pike-raw", 1600);
            regularWaterPool.AddItem("primitivesurvival:psfish-arcticchar-raw", 1600, "cold");
        }
        else
        {
            regularWaterPool.AddItem("fishing:catch-salmon-fish-fresh", 10000);
            regularWaterPool.AddItem("fishing:catch-catfish-fish-fresh", 2500);
        }
        regularWaterPool.AddItem("fishing:catch-neontetra-fish-fresh", 10000, "hot", "jungle");
        regularWaterPool.AddItem("fishing:catch-cavefish-fish-fresh", 1600, "cave");

        if (ModConfig.Loaded.flotsamEnabled)
        {
            regularWaterPool.AddItem("fishing:lootcrate-normal", 200, "inanimate");
            regularWaterPool.AddItem("fishing:lootcrate-copper", 40, "inanimate", "rare");
            regularWaterPool.AddItem("fishing:lootcrate-tungsten", 8, "inanimate", "legendary");

            regularWaterPool.AddItem("fishing:lootcrate-normal", 100, "inanimate", "cold");
            regularWaterPool.AddItem("fishing:lootcrate-copper", 20, "inanimate", "rare", "cold"); //50% more crates in arctic areas
            regularWaterPool.AddItem("fishing:lootcrate-tungsten", 4, "inanimate", "legendary", "cold");
        }

        //Salt pool
        if (api.ModLoader.IsModEnabled("primitivesurvival"))
        {
            saltWaterPool.AddItem("primitivesurvival:psfish-salmon-raw", 6000);
            saltWaterPool.AddItem("primitivesurvival:psfish-catfish-raw", 2000);
            saltWaterPool.AddItem("fishing:catch-redsnapper-fish-fresh", 2000);
        }
        else
        {
            saltWaterPool.AddItem("fishing:catch-salmon-fish-fresh", 6000);
            saltWaterPool.AddItem("fishing:catch-catfish-fish-fresh", 2000);
            saltWaterPool.AddItem("fishing:catch-redsnapper-fish-fresh", 2000);
        }
        saltWaterPool.AddItem("fishing:catch-mackerel-fresh", 4000, "mackerel");
        saltWaterPool.AddItem("fishing:catch-bluefintuna-fresh", 50, "cannibal", "rare");
        saltWaterPool.AddItem("fishing:catch-bullshark-fresh", 50, "carnivore", "rare");
        saltWaterPool.AddItem("fishing:edibleseaweed", 500, "inanimate", "cold");

        if (ModConfig.Loaded.flotsamEnabled)
        {
            saltWaterPool.AddItem("fishing:lootcrate-normal", 400, "inanimate");
            saltWaterPool.AddItem("fishing:lootcrate-copper", 80, "inanimate", "rare"); //Double crates in salt water
            saltWaterPool.AddItem("fishing:lootcrate-tungsten", 16, "inanimate", "legendary");

            saltWaterPool.AddItem("fishing:lootcrate-normal", 200, "inanimate", "cold");
            saltWaterPool.AddItem("fishing:lootcrate-copper", 40, "inanimate", "rare", "cold"); //50% more crates in arctic areas
            saltWaterPool.AddItem("fishing:lootcrate-tungsten", 8, "inanimate", "legendary", "cold");
        }
        
        //Boiling pool
        if (api.ModLoader.IsModEnabled("primitivesurvival"))
        {
            boilingWaterPool.AddItem("primitivesurvival:psfish-mutant-raw", 500);
        }
        boilingWaterPool.AddItem("game:stone-limestone", 100, "inanimate");

        //Lava pool
        lavaPool.AddItem("game:stone-obsidian", 500, "inanimate");
        lavaPool.AddItem("game:stone-basalt", 500, "inanimate");
        lavaPool.AddItem("fishing:lootcrate-tungsten", 1, "inanimate");

        string[] junk = { "game:stick", "game:stone-obsidian", "game:stone-granite", "game:drygrass", "game:dirtygravel-dry-plain" };
        for (int i = 0; i < junk.Length; i++)
        {
            regularWaterPool.AddItem(junk[i], 10000, "junk");
            saltWaterPool.AddItem(junk[i], 10000, "junk");
        }

        //----------CRATES----------
        string[] rotwalkerClothes = { "face-rotwalker", "foot-rotwalker", "head-rotwalker", "lowerbody-rotwalker", "shoulder-rotwalker", "upperbodyover-rotwalker" };
        for (int i = 0; i < rotwalkerClothes.Length; i++)
        {
            normalCrate.AddItem("game:clothes-" + rotwalkerClothes[i], 5);
        }

        string[] forgottennobleClothes = { "face-forgotten", "foot-forgotten", "hand-forgotten", "lowerbody-forgotten", "neck-forgotten", "upperbody-forgotten", "upperbodyover-forgotten" };
        for (int i = 0; i < forgottennobleClothes.Length; i++)
        {
            copperCrate.AddItem("game:clothes-" + forgottennobleClothes[i], 5);
        }

        string[] kingClothes = { "foot-king", "head-crown", "hand-king", "lowerbody-king", "upperbody-king", "upperbodyover-king", "neck-king", "waist-king" };
        for (int i = 0; i < kingClothes.Length; i++)
        {
            tungstenCrate.AddItem("game:clothes-" + kingClothes[i], 5);
        }

        string[] jonasParts = { "jonasframes-gearbox01", "jonasframes-gearbox02", "jonasframes-oscillator01", "jonasframes-spring01", "jonasframes-joint01", "jonasframes-gears01", "jonasframes-gears02"
                              , "jonasparts-connector01", "jonasparts-cylinder01", "jonasparts-cylinder02", "jonasparts-tank01", "jonasparts-tank02", "jonasparts-valve01" };
        for (int i = 0; i < jonasParts.Length; i++)
        {
            copperCrate.AddItem("game:" + jonasParts[i], 5);
        }

        string[] gems = { "gem-diamond-rough", "gem-emerald-rough", "gem-olivine_peridot-rough" };
        for (int i = 0; i < gems.Length; i++)
        {
            normalCrate.AddItem(gems[i], 2);
            copperCrate.AddItem(gems[i], 2);
            tungstenCrate.AddItem(gems[i], 2);
        }

        //Normal crate pool----------
        //Common
        normalCrate.AddItem("game:stick", 6, 24, 100);
        normalCrate.AddItem("game:flaxtwine", 4, 8, 100);
        normalCrate.AddItem("game:bandage-clean", 3, 6, 100);
        normalCrate.AddItem("game:bone", 2, 4, 100);

        //Uncommon
        normalCrate.AddItem("game:lime", 6, 24, 50);
        normalCrate.AddItem("game:peatbrick", 6, 24, 100);
        normalCrate.AddItem("game:beenade-closed", 2, 4, 50);
        normalCrate.AddItem("game:arrow-copper", 6, 18, 50);
        normalCrate.AddItem("game:gear-rusty", 2, 4, 50);
        normalCrate.AddItem("game:supportchain", 4, 8, 50);
        normalCrate.AddItem("game:metal-parts", 4, 8, 50);
        normalCrate.AddItem("game:burnedbrick-red", 12, 24, 50);
        normalCrate.AddItem("game:shingle-burned-red", 12, 24, 50);
        normalCrate.AddItem("game:fruit-breadfruit", 4, 8, 50);
        normalCrate.AddItem("game:ore-bituminouscoal", 4, 8, 50);
        normalCrate.AddItem("game:hide-raw-large", 4, 8, 50);
        normalCrate.AddItem("game:log-placed-aged-ud", 6, 18, 50);

        //Rare
        normalCrate.AddItem("game:ore-rich-malachite-greenmarble", 4, 8, 20);
        normalCrate.AddItem("game:gear-temporal", 20);
        normalCrate.AddItem("game:clothes-head-snow-goggles", 5);
        normalCrate.AddItem("game:treeseed-ebony", 4, 8, 5);
        normalCrate.AddItem("game:treeseed-purpleheart", 4, 8, 5);
        normalCrate.AddItem("game:seeds-peanut", 4, 8, 5);
        normalCrate.AddItem("game:clutter-skull/humanoid", 5);

        //Copper crate pool----------
        //Common
        copperCrate.AddItem("game:gear-rusty", 4, 6, 100);
        copperCrate.AddItem("game:linen-normal-down", 2, 4, 100);
        copperCrate.AddItem("game:ingot-copper", 2, 4, 100);
        copperCrate.AddItem("game:rope", 6, 24, 100);
        copperCrate.AddItem("game:resin", 4, 8, 100);
        copperCrate.AddItem("game:mushroom-puffball-normal", 4, 8, 100);

        //Uncommon
        copperCrate.AddItem("game:candle", 4, 8, 50);
        copperCrate.AddItem("game:leather", 4, 8, 50);
        copperCrate.AddItem("game:cheese-cheddar-4slice", 50);
        copperCrate.AddItem("game:metalplate-electrum", 50);
        copperCrate.AddItem("game:metalnailsandstrips-copper", 4, 8, 50);
        copperCrate.AddItem("game:ore-alum", 4, 8, 50);
        
        //Rare
        copperCrate.AddItem("game:banner-banner", "hansa-wall-fish", 20);
        copperCrate.AddItem("game:banner-banner", "hansa-wall-keys", 20);
        copperCrate.AddItem("game:banner-banner", "hansa-wall-largefish", 20);
        copperCrate.AddItem("game:banner-banner", "hansa-wall-ship", 20);
        copperCrate.AddItem("game:banner-banner", "hansa-wall-town", 20);
        copperCrate.AddItem("game:jonas-lamp", "lightsource/on/lumi-floor8", 10);
        copperCrate.AddItem("game:jonas-lamp", "lightsource/on/gas-floor2", 10);
        copperCrate.AddItem("game:clutter", "globe2", 10);
        copperCrate.AddItem("game:clutter-hourglass", 10);
        copperCrate.AddItem("game:storagevessel-seasalt", 10);
        copperCrate.AddItem("game:armor-head-antique-blackguard-broken", 10);
        copperCrate.AddItem("game:armor-body-antique-blackguard-broken", 10);
        copperCrate.AddItem("game:armor-legs-antique-blackguard-broken", 10);
        copperCrate.AddItem("game:drycarcass-humanoid1", 10);
        copperCrate.AddItem("game:paperlantern-on", 10);
        copperCrate.AddItem("game:clutter-art/statuette-horus", 5);
        copperCrate.AddItem("game:schematic-glider", 1);
        copperCrate.AddItem("game:clutter", "golem-head", 2);
        copperCrate.AddItem("game:blade-forlorn-iron", 2);
        copperCrate.AddItem("game:ember", 2);
        
        //Tungsten crate pool----------
        //Common
        tungstenCrate.AddItem("game:gear-rusty", 6, 24, 100);
        tungstenCrate.AddItem("game:gear-temporal", 2, 4, 100);

        //Uncommon
        tungstenCrate.AddItem("game:ingot-gold", 4, 6, 30);
        tungstenCrate.AddItem("game:ingot-silver", 4, 6, 30);
        
        tungstenCrate.AddItem("game:banner-banner", "hansa-ground-fish", 30);
        tungstenCrate.AddItem("game:banner-banner", "hansa-ground-keys", 30);
        tungstenCrate.AddItem("game:banner-banner", "hansa-ground-largefish", 30);
        tungstenCrate.AddItem("game:banner-banner", "hansa-ground-ship", 30);
        tungstenCrate.AddItem("game:banner-banner", "hansa-ground-town", 30);

        //Rare
        tungstenCrate.AddItem("fishing:fishingpole-tungsten", 20);
        tungstenCrate.AddItem("game:armor-head-antique-forlorn-broken", 10);
        tungstenCrate.AddItem("game:armor-body-antique-forlorn-broken", 10);
        tungstenCrate.AddItem("game:armor-legs-antique-forlorn-broken", 10);
        tungstenCrate.AddItem("game:ore-cinnabar", 12, 24, 10);
        tungstenCrate.AddItem("game:ore-lapislazuli", 12, 24, 10);
        tungstenCrate.AddItem("game:ember", 10);
        tungstenCrate.AddItem("game:claybrickchimney-blue-lit-ns", 10);
        tungstenCrate.AddItem("game:claybrickchimney-brown-lit-ns", 10);
        tungstenCrate.AddItem("game:claybrickchimney-fire-lit-ns", 10);
        tungstenCrate.AddItem("game:claybrickchimney-red-lit-ns", 10);
        tungstenCrate.AddItem("game:quiver", 10);
        tungstenCrate.AddItem("game:clothes-head-mianguan", 1);
        tungstenCrate.AddItem("game:creature-bear-female-sun", 1);
        tungstenCrate.AddItem("game:metalblock-new-riveted-uranium", 2, 4, 1);
    }

    public void debugPool(WeightedRandomSelector selector)
    {
        sapi.BroadcastMessageToAllGroups("Debugging pools", EnumChatType.Notification);
        foreach (LootObject loot in selector.items)
        {
            AssetLocation newAsset = new AssetLocation(loot.Name);

            if (sapi.World.GetItem(newAsset) == null && sapi.World.GetBlock(newAsset) == null)
            {
                sapi.BroadcastMessageToAllGroups(loot.Name + " is null", EnumChatType.Notification);
            }
        }
    }

    public void debugPools()
    {
        debugPool(regularWaterPool);
        debugPool(saltWaterPool);
        debugPool(boilingWaterPool);
        debugPool(lavaPool);
        debugPool(normalCrate);
        debugPool(copperCrate);
        debugPool(tungstenCrate);
    }

    //----------SOUND
    public void playSound(FishingSoundMessage networkMessage)
    {
        //Skip sounds too far away
        Entity entity = capi.World.GetEntityById(networkMessage.entityId);
        if (entity == null || capi.World.Player?.Entity?.Pos.DistanceTo(entity?.Pos) > 128) return;

        IPlayer byPlayer = null;
        if (entity is EntityPlayer) byPlayer = entity.World.PlayerByUid(((EntityPlayer)entity).PlayerUID);
        switch (networkMessage.type)
        {
            case 1:
                entity.World.PlaySoundAt(new AssetLocation("fishing:sounds/cast"), entity, byPlayer, false, 16);
                break;
            case 2:
                entity.World.PlaySoundAt(new AssetLocation("fishing:sounds/reel"), entity, byPlayer, false, 16);
                break;
            case 3:
                entity.World.PlaySoundAt(new AssetLocation("fishing:sounds/splash"), entity, null, true, 64);
                break;
            case 4:
                entity.World.PlaySoundAt(new AssetLocation("fishing:sounds/rock"), entity, null, true, 128, 6);
                break;
        }
    }

    public void sendSound(int type, long id)
    {
        serverChannel.BroadcastPacket(new FishingSoundMessage()
        {
            type = type,
            entityId = id
        });
    }

    public void receiveParticles(ParticlesMessage networkMessage)
    {
        EntityFishingHook hook = (EntityFishingHook)capi.World.GetEntityById(networkMessage.entityId);
        if (hook != null)
        {
            hook.spawnParticles = true;
            hook.particleColor = networkMessage.color;
        }
    }

    //----------NETWORK
    public void syncHooks()
    {
        //Clean up
        foreach (KeyValuePair<long, long> entry in hookMapIds)
        {
            long[] toRemove = { };
            if (sapi.World.GetEntityById(entry.Value) == null || sapi.World.GetEntityById(entry.Value) == null)
            {
                toRemove.AddItem(entry.Key);
            }
            for (int i = 0; i < toRemove.Length; i++) 
            {
                if (sapi.World.GetEntityById(hookMapIds.Get(toRemove[i])) != null) sapi.World.GetEntityById(hookMapIds.Get(toRemove[i])).Die();
                hookMapIds.Remove(toRemove[i]);
            }
        }
        serverChannel.BroadcastPacket(new SyncHooksMessage()
        {
            idMessage = hookMapIds
        });
    }

    public void OnServerMessage(SyncHooksMessage networkMessage)
    {
        hookMapIds.Clear();
        foreach (KeyValuePair<long, long> entry in networkMessage.idMessage)
        {
            hookMapIds.Remove(entry.Key);
            hookMapIds.Add(entry.Key, entry.Value);
        }
    }

    //Config
    public override void StartPre(ICoreAPI api)
    {
        string cfgFileName = "EXTREMEFISHING.json";
        try
        {
            ModConfig fromDisk;
            if ((fromDisk = api.LoadModConfig<ModConfig>(cfgFileName)) == null)
            { api.StoreModConfig(ModConfig.Loaded, cfgFileName); }
            else
            { ModConfig.Loaded = fromDisk; }
        }
        catch
        {
            api.StoreModConfig(ModConfig.Loaded, cfgFileName);
        }
        base.StartPre(api);
    }
}

[ProtoContract]
public class SyncHooksMessage
{
    [ProtoMember(1)]
    public Dictionary<long, long> idMessage;
}

[ProtoContract]
public class FishingSoundMessage
{
    [ProtoMember(1)]
    public int type;

    [ProtoMember(2)]
    public long entityId;
}

[ProtoContract]
public class ParticlesMessage
{
    [ProtoMember(1)]
    public long entityId;

    [ProtoMember(2)]
    public int color;
}

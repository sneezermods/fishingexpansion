﻿using System;
using System.Collections.Generic;
using System.Text;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.API.Util;

public class ItemFishingPole : Item
{
    //Map of entity ids
    public Dictionary<long, long> hookMapIds;

    FishingMod mod;

    //API
    ICoreServerAPI sapi;

    public override void OnLoaded(ICoreAPI api)
    {
        base.OnLoaded(api);

        mod = (FishingMod)api.ModLoader.GetModSystem("FishingMod");

        //Load hook map
        hookMapIds = mod.hookMapIds;

        if (api.World.Side == EnumAppSide.Server)
        {
            sapi = api as ICoreServerAPI;
        }
    }

    public override void OnHeldInteractStart(ItemSlot slot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel, bool firstEvent, ref EnumHandHandling handling)
    {
        base.OnHeldInteractStart(slot, byEntity, blockSel, entitySel, firstEvent, ref handling);

        if (api.World.Side == EnumAppSide.Server)
        {
            EntityFishingHook hook = (EntityFishingHook)api.World.GetEntityById(hookMapIds.Get(byEntity.EntityId));

            if (hook != null && !hook.Alive)
            {
                hookMapIds.Remove(byEntity.EntityId);
                hook = null;
            }

            if (hook != null)
            {
                hook.beingRetrieved = true;
                mod.sendSound(2, byEntity.EntityId); //Send reel sound
                handling = EnumHandHandling.NotHandled;
            }

            else
            {
                handling = EnumHandHandling.PreventDefault;
            }
        }

        if (api.World.Side == EnumAppSide.Client)
        {
            handling = EnumHandHandling.PreventDefault;
        }
    }

    public override bool OnHeldInteractStep(float secondsUsed, ItemSlot slot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel)
    {
        //Simulates rod casting movement
        ModelTransform tf = new ModelTransform();
        tf.EnsureDefaultValues();
        tf.Origin.Set(0, -1, 0);
        tf.Rotation.X = Math.Min(40, (secondsUsed * 20) * (slot.Itemstack.Collectible.Attributes["castingPower"].AsFloat() * 5));
        byEntity.Controls.UsingHeldItemTransformAfter = tf;
        return true;
    }

    public override void OnHeldInteractStop(float secondsUsed, ItemSlot slot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel)
    {
        base.OnHeldInteractStop(secondsUsed, slot, byEntity, blockSel, entitySel);

        if (byEntity.World.Side == EnumAppSide.Server)
        {
            //Return if no bait
            ItemSlot bait = GetBait(byEntity);
            if (bait == null) return;

            //Return if pole breaks
            slot.Itemstack.Collectible.DamageItem(sapi.World, byEntity, slot);
            if (slot.Itemstack.Collectible.Durability == 0) return;

            //Initialize entity
            EntityProperties type;
            switch (bait.Itemstack.Collectible.LastCodePart())
            {
                case "net":
                    type = api.World.GetEntityType(new AssetLocation("fishing:fishinghook-net"));
                    break;
                case "jonaslure":
                    type = api.World.GetEntityType(new AssetLocation("fishing:fishinghook-jonaslure"));
                    break;
                default:
                    type = api.World.GetEntityType(new AssetLocation("fishing:fishinghook-hook"));
                    break;
            }
            EntityFishingHook fishinghook = (EntityFishingHook)api.ClassRegistry.CreateEntity(type);
            fishinghook.castedBy = (EntityPlayer)byEntity;
            fishinghook.bait = bait.TakeOut(1); //Move 1 of them into the fishing hook stack
            bait.MarkDirty();
            fishinghook.polePower = slot.Itemstack.Collectible.Attributes["fishingPower"].AsInt(); //Pole power
            fishinghook.reelSpeed = slot.Itemstack.Collectible.Attributes["reelSpeed"].AsDouble(); //Reel speed

            //Calculate and set velocity
            Vec3d pos = byEntity.ServerPos.XYZ.Add(0, byEntity.LocalEyePos.Y, 0);
            Vec3d aheadPos = pos.AheadCopy(1, byEntity.ServerPos.Pitch, byEntity.ServerPos.Yaw);
            Vec3d velocity = (aheadPos - pos) * Math.Min(secondsUsed * slot.Itemstack.Collectible.Attributes["castingPower"].AsFloat(), 1f);
            fishinghook.ServerPos.SetPos(byEntity.ServerPos.BehindCopy(0.20).XYZ.Add(0, byEntity.LocalEyePos.Y, 0));
            fishinghook.ServerPos.Motion.Set(velocity);

            //Finish
            fishinghook.ServerPos.SetFrom(fishinghook.ServerPos);
            fishinghook.World = byEntity.World;
            api.World.SpawnEntity(fishinghook);

            //Remove old entities
            hookMapIds.Remove(byEntity.EntityId);
            hookMapIds.Add(byEntity.EntityId, fishinghook.EntityId);

            //Send cast sound
            mod.sendSound(1, byEntity.EntityId);

            //Sync
            mod.syncHooks();
        }
    }

    protected ItemSlot GetBait(EntityAgent byEntity)
    {
        ItemSlot slot = null;
        byEntity.WalkInventory((invslot) =>
        {
            if (invslot is ItemSlotCreative) return true;
            //if (invslot.Itemstack != null && invslot.Itemstack.Collectible.Code.Path.StartsWith("bait-"))
            if (invslot.Itemstack?.Collectible?.Attributes?.KeyExists("baitPower") == true)
            {
                slot = invslot;
                return false;
            }
            return true;
        });
        return slot;
    }
    
    public override void GetHeldItemInfo(ItemSlot inSlot, StringBuilder dsc, IWorldAccessor world, bool withDebugInfo)
    {
        base.GetHeldItemInfo(inSlot, dsc, world, withDebugInfo);

        if (inSlot.Itemstack.Collectible.Attributes == null) return;

        dsc.AppendLine("Fishing Power: " + inSlot.Itemstack.Collectible.Attributes["fishingPower"].AsInt());
        dsc.AppendLine("Casting Power: " + inSlot.Itemstack.Collectible.Attributes["castingPower"].AsFloat());
        dsc.AppendLine("Reel Speed: " + inSlot.Itemstack.Collectible.Attributes["reelSpeed"].AsDouble());
    }

    public override string GetHeldTpUseAnimation(ItemSlot activeHotbarSlot, Entity byEntity)
    {
        return null;
    }
}
